<?php
/**
 * server
 * copyright (c) 2014 abie
 *
 * @author abie
 * @date 11/20/14 9:43 AM
 */

class ServerException extends Exception {
    /**
     *
     * @param string $message [optional] The Exception message to throw.
     * @param int $code [optional] The Exception code.
     * @param Exception $previous [optional] The previous exception used for the exception chaining. Since 5.3.0
     */
    public function __construct($msg, $code = 400, $pref=null) {
        parent::__construct($msg, $code, $pref);
    }
} 