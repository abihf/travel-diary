<?php
/**
 * server
 * copyright (c) 2014 abie
 *
 * @author abie
 * @date 11/20/14 3:40 PM
 */

interface IService {
    /**
     * @param Server $server
     */
    public function __construct($server);

    /**
     * @param string $action
     * @param mixed $param
     * @return mixed
     */
    public function runAction($action, $param);
} 