<?php
/**
 * server
 * copyright (c) 2014 abie
 *
 * @author abie
 * @date 11/20/14 9:56 AM
 */

class Session {
    private $started = false;

    private function start() {
        if (!$this->started) {
            session_name("sid");
            session_start();
        }
    }

    public function getId() {
        $this->start();
        return session_id();
    }

    public function setId($id) {
        session_id($id);
    }

    public function destroy() {
        $this->start();
        session_destroy();
        $this->started = false;
    }

    public function get($name, $default = null) {
        $this->start();
        return isset($_SESSION[$name]) ? $_SESSION[$name] : $default;
    }

    public function set($name, $value) {
        $this->start();
        $_SESSION[$name] = $value;
    }
} 