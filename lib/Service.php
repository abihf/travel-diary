<?php

abstract class Service implements IService {

    /**
     * @var Server
     */
    protected $server;


    protected $needAuth = false;

    protected $auth;

    /**
     * @param Server $server
     */
    public function __construct($server) {
        $this->server = $server;
        $this->auth = $server->getAuth();
    }

    /**
     * @param $action
     * @param $param
     * @return mixed
     * @throws Exception
     */
    public function runAction($action, $param) {
        $this->checkAuth($action);
        $methodName = 'action' . $action;
        $method = [&$this, $methodName];
        if (is_callable($method)) {
            $reflection = new ReflectionMethod($this, $methodName);
            $invokeParam = [];
            foreach ($reflection->getParameters() as $p) {
                $name = $p->getName();
                if (isset($param[$name])) {
                    $invokeParam[$name] = $param[$name];
                }
                elseif (! $p->isOptional()) {
                    throw new Exception('Invalid number of arguments. Required ' . $name);
                }

            }
            return $reflection->invokeArgs($this, $invokeParam);
        }
        else {
            throw new Exception("Action not found");
        }
    }

    protected function checkAuth($action) {
        if ($this->needAuth &&
            ($this->needAuth == '*' || (is_array($this->needAuth) && in_array($action, $this->needAuth))) &&
            $this->auth->getUid() == 0
        ) {
           throw new ServerException('Not Authenticated');
        }
    }

}
