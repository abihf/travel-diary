<?php
/**
 * server
 * copyright (c) 2014 abie
 *
 * @author abie
 * @date 11/29/14 4:22 PM
 */

namespace db;


class Scheme {
    public function escapeTableName($name) {
        return '"' . $name . '"';
    }

    public function escapeColumnName($name) {
        return '"' . $name . '"';
    }

    public function escapeStringValue($value) {
        return '\'' . addslashes($value) . '\'';
    }
} 