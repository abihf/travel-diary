<?php
/**
 * server
 * copyright (c) 2014 abie
 *
 * @author abie
 * @date 11/29/14 4:18 PM
 */
namespace db;
use \Logger;

class Database extends \PDO {

    protected $scheme;

    /**
     * (PHP 5 &gt;= 5.1.0, PECL pdo &gt;= 0.1.0)<br/>
     * Creates a PDO instance representing a connection to a database
     *
     * @link http://php.net/manual/en/pdo.construct.php
     * @param string $dsn
     * @param string $username [optional]
     * @param string $password
     * @param string $options [optional]
     */
    public function __construct($dsn, $username = null, $password = null, $options = null) {
        parent::__construct($dsn, $username, $password, $options);
        $this->scheme = new Scheme();
    }

      
    /**
     * @param $table
     * @param string $column
     * @param string $condition
     * @param array $params
     * @param string $options
     * @return \PDOStatement
     */
    public function select($table, $column = '*', $condition = '1', $params = [], $options = '') {
        if ($column == null)
            $column = '*';
        elseif (is_array($column)) {
            $column = implode(', ', array_map([$this->scheme, 'escapeColumnName'], $column));
        }
        $table = $this->scheme->escapeTableName($table);
        
        $sql = "SELECT $column FROM $table WHERE $condition $options";
        $stmt = $this->prepare($sql);
        Logger::log('DB:SELECT: {%s} {%s}', $sql, json_encode($params));
        if (!$stmt)
          throw new \Exception('DB PREPARE ERROR: ' . implode(' :: ', $this->errorInfo()));
//        foreach ($params as $k => $v)
//            $stmt->bindValue($k, $v);
        if (!$stmt->execute($params)) 
          throw new \Exception('DB EXECUTE ERROR: ' . implode(' :: ', $stmt->errorInfo()));
        return $stmt;
    }

    /**
     * @param string $table
     * @param array $data
     * @return int
     */
    public function insert($table, $data) {
        $columns = [];
        $values = [];
        foreach ($data as $k => $v) {
            $columns[] = $this->scheme->escapeColumnName($k);
            $values[] = $this->scheme->escapeStringValue($v);
        }
        $sql = 'INSERT INTO ' . $this->scheme->escapeTableName($table) . ' (' . implode(', ', $columns) .
            ') VALUES (' . implode(', ', $values) . ');';
        Logger::log('DB:INSERT: {%s}', $sql);
        return $this->exec($sql);
    }


    public function update($table, $data, $condition = '1', $params = [], $options = '') {
        $sets = [];
        foreach ($data as $k => $v) {
            $sets[] = $this->scheme->escapeColumnName($k) . '=' . $this->scheme->escapeStringValue($v);
        }
        $sql = 'UPDATE ' . $this->scheme->escapeTableName($table) . ' SET' . implode(', ', $sets) .
            " WHERE $condition $options";
        $stmt = $this->prepare($sql);
        Logger::log('DB:UPDATE: {%s} {%s}', $sql, json_encode($params));
        return $stmt->execute($params);
    }

    public function insertOrUpdate($table, $pk, $data) {
        $_data= [];
        $_data = array_merge($_data, $data, $pk);
        if ($this->insert($table, $_data)) {
            return true;
        }
        else {
            $conditions = [];
            foreach ($pk as $k => $v) {
                $conditions[] = $this->scheme->escapeColumnName($k) . '=' . $this->scheme->escapeStringValue($v);
            }
            return $this->update($table, $data, implode(' AND ', $conditions));
        }
    }

    public function delete($table, $condition = '1', $params = []) {
        $sql = 'DELETE FROM ' . $this->scheme->escapeTableName($table) . ' WHERE ' . $condition;
        Logger::log('DB:UPDATE: {%s} {%s}', $sql, json_encode($params));
        $stmt = $this->prepare($sql);
        return $stmt->execute($params);
    }

    public function errorString($sep = ' :: ') {
        return implode($sep, $this->errorInfo());
    }

} 
