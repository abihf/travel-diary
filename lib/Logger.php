<?php

class Logger {
  
  public static $logFile = __DIR__ . '/../log';
  
  const LOG = 'log';
  const ERROR = 'err';
  const WARNING = 'wrn';
  
  private  static function write($type, $msg, $args) {
    $f = fopen(self::$logFile, 'a');
    fwrite($f, $type . ':: ' . vsprintf($msg, $args) . "\n");
    fclose($f);
  }
  
  public static function log($msg, $args = null) {
    $args = func_get_args();
    $msg = array_shift($args);
    self::write(self::LOG, $msg, $args);
  }
  
  public static function error($msg, $args = null) {
    $args = func_get_args();
    $msg = array_shift($args);
    self::write(self::ERROR, $msg, $args);
  }
  
  public static function warning($msg, $args = null) {
    $args = func_get_args();
    $msg = array_shift($args);
    self::write(self::WARNING, $msg, $args);
  }

}
