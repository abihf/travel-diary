<?php

/**
 * Class Server
 */
class Server
{

    public $config;

    /**
     * @var Auth
     */
    private $auth;

    /**
     * @var Session
     */
    private $session;

    /**
     * @var PDO
     */
    private $db;

    private $data;

    public static $instance;


    public function __construct($config) {
        $this->config = $config;
        $this->auth = new Auth($this);
        self::$instance = $this;
    }

    /**
     *
     */
    public function run()
    {
        Logger::log("Request: %s %s", $_SERVER['REQUEST_METHOD'], $_SERVER['REQUEST_URI']);
        try {
            $path = $_SERVER['REQUEST_URI'];
            if (($idx = strpos($path, '?')) !== false) {
                $path = substr($path, 0, $idx);
            }

            $respond = ['status' => 'OK', 'result' => $this->handleRequest($path, $_GET)];
        } catch (Exception $e) {
            $respond = [
                'status' => 'ERR',
                'error' => [
                    'msg' => $e->getMessage(),
                    'code' => $e->getCode(),
                    'file' => $e->getFile(),
                    'line' => $e->getLine(),
                    'trace' => $e->getTrace(),
                ]
            ];
        }
        header('Content-type: application/json');
        $text = json_encode($respond);
        Logger::log("Response: %s", $text);
        echo $text;

    }

    /**
     * @param $path
     * @param $param
     * @return mixed
     * @throws Exception
     */
    private function handleRequest($path, $param)
    {
        $pathArray = explode('/', trim($path, '/'));
        if (count($pathArray) != 2) {
            throw new Exception("Invalid Request; $path : " . count($pathArray), 400);
        }

        $service = $this->getService($pathArray[0]);
        return $service->runAction($pathArray[1], $param);
    }

    /**
     * @param $serviceName
     * @return Service
     * @throws Exception
     */
    private function getService($serviceName) {
        $className = str_replace('-', '', ucwords($serviceName)) . 'Service';
        if (class_exists($className) && is_subclass_of($className, 'IService')) {
            return new $className($this);
        }
        else {
            throw new Exception("Service not found");
        }
    }

    public function readData($field = null) {
        if ($this->data == null) {
            $contentType = isset($_SERVER['HTTP_CONTENT_TYPE']) ? trim(explode(';', $_SERVER['HTTP_CONTENT_TYPE'])[0]) : '*';
            $data = null;
            switch ($contentType) {
                case 'application/json':
                case 'text/json':
                    $data = json_decode(file_get_contents('php://input'), true);
                    break;

                case 'multipart/form-data':
                    $data = [];
                    $data = array_merge($data, $_POST, $_FILES);
                    break;

                default:
                    $data = $_POST;
            }
            $this->data = $data;
            Logger::log('Data: %s', json_encode($data));
        }
        if ($field && is_string($field)) {
          $field = explode(',', $field);
        }
        if ($field && is_array($field)) {
            $result = [];
            $data = $this->data;
            foreach ($field as $f) {
                $f = trim($f);
                if ($f[0] == ':') {
                    $optional = true;
                    $f = substr($f, 1);
                } else {
                    $optional = false;
                }
                if (isset($data[$f])) {
                    $result[$f] = $data[$f];
                } elseif ($optional) {
                    $result[$f] = null;
                } else {
                    throw new Exception('Missing required data: ' . $f);
                }
            }
            return $result;
        } else {
            return $this->data;
        }
    }

    /**
     * @return Session
     */
    public function getSession() {
        if ($this->session == null) {
            return $this->session = new Session();
        }
        return $this->session;
    }

    /**
     * @return \db\Database
     */
    public function getDb() {
        if ($this->db == null) {
            $conf = $this->config['db'];
            isset($conf['user']) || $conf['user'] = null;
            isset($conf['password']) || $conf['password'] = null;
            return $this->db = new \db\Database($conf['dsn'], $conf['user'], $conf['password']);
        }
        return $this->db;
    }

    /**
     * @return Auth
     */
    public function getAuth()
    {
        return $this->auth;
    }


}
