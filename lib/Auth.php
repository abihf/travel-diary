<?php
/**
 * server
 * copyright (c) 2014 abie
 *
 * @author abie
 * @date 11/20/14 10:46 AM
 */

class Auth {

    /**
     * @var Server
     */
    private $server;

    /**
     * @param Server $server
     */
    function __construct($server)
    {
        $this->server = $server;
    }


    public function login($uid) {
        $this->server->getSession()->set('uid', $uid);
    }

    public function logout() {
        $this->server->getSession()->destroy();
    }

    public function getUid() {
        if (isset($_GET['uid'])) return $_GET['uid'];
        if (isset($_POST['uid'])) return $_POST['uid'];
        return $this->server->getSession()->get('uid');
    }

} 