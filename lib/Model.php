<?php
/**
 * server
 * copyright (c) 2014 abie
 *
 * @author abie
 * @date 11/26/14 6:45 PM
 */

abstract class Model {

    protected $needAuth = '*';
    /** @var  PDO */
    static protected $db;

    protected $pk = 'id';

    protected $_data;

    protected $_new = true;

    /**
     * @return string
     */
    abstract static function getTableName();

    /**
     * @param $pk
     */
    public function getByPk($pk) {
        return $this->getDb("$pk = :pk", [':pk' => $pk]);
    }

    protected static function getDb() {
        if (self::$db == null) {
            return self::$db = Server::$instance->getDb();
        }
        return self::$db;
    }



    public static function find($condition, $params = [], $options = '') {
        $table = self::getTableName();
        $stmt = self::getDb()->query("SELECT * FROM \"$table\" WHERE $condition $options ");
        foreach ($params as $k => $v)
            $stmt->bindValue($k, $v);
        return $stmt->fetchAll();
    }

    public function get($condition, $params = [], $options = '') {
        $options .= ' LIMIT 1';
        $results = $this->find($condition, $params, $options);
        if ($results && isset($results[0]))
            return $results[0];
        return null;
    }


    public function actionSync($last) {
        $db = $this->server->getDb();

        $stmt = $db->query(sprintf('SELECT * FROM "%s" WHERE ', $this->getTableName(), $this->pk));
        //$stmt->bindValue(':pk', $id);
        $data =  $stmt->fetchAll();
        $stmt->closeCursor();
        return $this->filterResult($data);
    }

    public function actionUpdate($id) {
        $db = $this->server->getDb();
        $data = $this->filterData($_POST, 'UPDATE');

        $sets = [];
        foreach ($data as $key => $value) {
            $sets[] = pg_escape_identifier($key) . '=' . pg_escape_string($value);
        }
        $stmt = $db->prepare(sprintf('UPDATE FROM %s SET %s WHERE %s = :pk',
                pg_escape_identifier($this->getTableName()),
                implode(',', $sets),
                pg_escape_identifier($this->pk)
            ));
        if (!$stmt) throw new Exception('DB:PrepareError: ' . $db->errorInfo()[2]);
        $stmt->bindValue(':pk', $id);

        if ($stmt->execute()) {
            return [
                'status' => 'OK', 'result' => true
            ];
        } else {
            $err = $stmt->errorInfo();
            throw new Exception('DB:ExecuteError: ' . $err[2]);
        }

    }

    public function actionInsert() {
        $db = $this->server->getDb();
        $data = $this->filterData($_POST, 'INSERT');

        $keys = [];
        $values = [];
        foreach ($data as $key => $value) {
            $keys[] = pg_escape_identifier($key);
            $values[] = pg_escape_string($value);
        }
        $stmt = $db->prepare(sprintf('INSERT INTO %s (%s) VALUES (%s)',
                pg_escape_identifier($this->getTableName()),
                implode(',', $keys), implode(',', $values)
            ));
        if (!$stmt) throw new Exception('DB:PrepareError: ' . $db->errorInfo()[2]);

        if ($stmt->execute()) {
            return [
                'status' => 'OK', 'result' => true
            ];
        } else {
            $err = $stmt->errorInfo();
            throw new Exception('DB:ExecuteError: ' . $err[2]);
        }

    }

    public function actionDelete($id) {
        $db = $this->server->getDb();
        $stmt = $db->prepare(sprintf('DELETE FROM %s WHERE %s = :pk', pg_escape_identifier($this->getTableName()), pg_escape_identifier($this->pk)));
        $stmt->bindValue(':pk', $id);
        if ($stmt->execute()) {
            return [
                'status' => 'OK', 'result' => true
            ];
        } else {
            $err = $stmt->errorInfo();
            throw new Exception('DB:ExecuteError: ' . $err[2]);
        }
    }

    protected function filterResult($data) {
        return $data;
    }

    protected function filterData($data, $method) {
        return $data;
    }

} 