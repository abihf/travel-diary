

CREATE TABLE "user" (
  "userId" serial PRIMARY KEY,
  "email" varchar(256) NOT NULL UNIQUE,
  "password" varchar(256) NOT NULL,  
  "name" varchar(64) NOT NULL,
  "address" varchar(512),
  "phone" varchar(32),
  "aboutMe" text,
  
  "lastModified" timestamp
);


CREATE TABLE "moment" (
  "momentId" varchar(32),
  "userId" int REFERENCES "user" ("userId") ON DELETE CASCADE,
  "location" varchar(256),
  "longitude" real,
  "latitude" real,
  "story" text,
  "time" timestamp,

  "lastModified" TIMESTAMP,
  "deletedTime" TIMESTAMP,

  PRIMARY KEY ("momentId", "userId")
);


CREATE TABLE "voice" (
  "voiceId" varchar(32),
  "momentId" varchar(32) NOT NULL ,
  "userId" int,
  "file" varchar(256) NOT NULL,

  "lastModified" TIMESTAMP,
  "deletedTime" TIMESTAMP,

  PRIMARY KEY ("voiceId", "userId"),
  FOREIGN KEY ("momentId", "userId") REFERENCES "moment" ("momentId", "userId")

);

CREATE TABLE "photo" (
  "photoId" varchar(32),
  "momentId" varchar(32) NOT NULL,
  "userId" int,
  "user_id" int,
  "file" varchar(256) NOT NULL,

  "lastModified" TIMESTAMP,
  "deletedTime" TIMESTAMP,

  PRIMARY KEY ("photoId", "userId"),
  FOREIGN KEY ("momentId", "userId") REFERENCES "moment" ("momentId", "userId")
);
