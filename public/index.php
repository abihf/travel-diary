<?php

ini_set('display_errors', 1);
error_reporting(E_ALL - E_STRICT - E_NOTICE);
include __DIR__ . '/../loader.php';
(new Server(include(__DIR__ . '/../config.php')))->run();
