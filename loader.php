<?php

class ClassLoader
{
    public static $includeDirs = array();

    /**
     * @param $className
     */
    public static function autoLoad($className) {
        foreach(self::$includeDirs as $dir) {
            $classFile = sprintf('%s/%s.php', $dir, str_replace('\\', '/', $className));
            //var_dump($classFile);
            if (file_exists($classFile)) {
                /** @noinspection PhpIncludeInspection */
                include $classFile;
                return;
            }
        }
    }
}

ClassLoader::$includeDirs[] = __DIR__ . '/lib/';
ClassLoader::$includeDirs[] = __DIR__ . '/services/';

spl_autoload_register(array('ClassLoader', 'autoLoad'));