<?php
/**
 * server
 * copyright (c) 2014 abie
 *
 * @author abie
 * @date 11/20/14 9:51 AM
 */

class UserService extends Service {

    /**
     * @return object
     * @throws Exception
     * @throws ServerException
     */
    public function actionLogin() {
        $db = $this->server->getDb();

        $data = $this->server->readData();
        $email = $data['email'];
        $password = $data['password'];

        $user = $db->select('user', '*', 'email = :email', [':email' => $email], 'LIMIT 1')
            ->fetchObject();

        if ($user && $user->password == crypt($password, $user->password)) {
            $this->auth->login($user->id);
            unset($user->password);
            return $user;
        }

        throw new ServerException('Invalid email or password');
    }

    public function actionLogout() {
        $this->auth->logout();
        return true;
    }

    /**
     * @throws Exception
     * @return boolean
     */
    public function actionRegister() {
        $data = $this->server->readData(['email', 'password', 'name']);
        $data['password'] = crypt($data['password']);

        $db = $this->server->getDb();
        if ($db->insert('user', $data)) {
            $id = $db->lastInsertId();
            $data['id'] = $id;
            $this->auth->login($id);
            return true;
        } else {
            throw new Exception("Register Error: " . implode("\n", $db->errorInfo()));
        }
    }

    /**
     * @return bool
     */
    public function actionUpdate() {
        return true;
    }

} 
