<?php
/**
 * server
 * copyright (c) 2014 abie
 *
 * @author abie
 * @date 11/29/14 7:15 PM
 */

class VoiceService extends Service {
    protected $needAuth = '*';

    public function actionSync($last) {
        $stmt = $this->server->getDb()
            ->select(
                'voice', 'voiceId',
                'userId = :uid AND (createdTime >= :lm OR deletedTime >= :lm)',
                [':uid' => $this->auth->getUid(), ':lm' => $last]
            );
        $results = [];
        foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $row) {
            $results[] = $row;
        }
        return $results;
    }

    private function getFileName($id) {
        return __DIR__ . '/../upload/' . $this->auth->getUid() . '/' . $id . '.jpg';
    }

    public function actionGet($id) {
        $file = $this->getFileName($id);
        if (file_exists($file))  {
            header('Content-type: image/jpeg');
            readfile($file);
            exit;
        } else {
            header('404 Not Found');
            throw new Exception('File not found');
        }
    }

    public function actionPost() {
        $data = $this->server->readData();
        $file = $this->getFileName($data['voiceId']);
        if (!file_exists(dirname($file))) mkdir(dirname($file), 0777, true);
        if (move_uploaded_file($_FILES['file'], $file)) {
            $db = $this->server->getDb();
            $pk = ['userId' => $this->auth->getUid(), 'voiceId' => $data['voiceId']];
            if (! $db->insertOrUpdate('moment', $pk, $data)) {
                throw new Exception($db->errorString());
            }
            return true;
        }
        else {
            throw new Exception('upload error');
        }

    }

    public function actionDelete($id) {
        $db = $this->server->getDb();
        $stmt = $db->prepare('UPDATE voice SET "deletedTime" = NOW() WHERE "voiceId" = :id AND "userId" = :uid');
        if (!$stmt->execute([':id' => $id, ':uid' => $this->auth->getUid()])) {
            throw new Exception($db->errorString());
        }
        return true;
    }
} 