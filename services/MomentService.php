<?php
/**
 * server
 * copyright (c) 2014 abie
 *
 * @author abie
 * @date 11/29/14 5:05 PM
 */

class MomentService extends Service {
    protected $needAuth = '*';

    public function actionSync($last) {
        $stmt = $this->server->getDb()
            ->select(
                'moment',
                '"momentId"',
                '"userId" = :uid AND "lastModified" >= :lm AND "deletedTime" IS NULL',
                [':uid' => $this->auth->getUid(), ':lm' => $last]
            );
        $results = [];

        foreach ($stmt as $row) {
            $results[] = $row[0];
        }
        return $results;
    }

    public function actionGet($id) {
        $result = $this->server->getDb()
            ->select(
                'moment',
                '*',
                '"userId" = :uid AND "momentId" = :id',
                [':uid' => $this->auth->getUid(), ':id' => $id]
            )
            ->fetchObject();
        return $result;
    }

    public function actionPost() {
        $data = $this->server->readData();
        $db = $this->server->getDb();
        $pk = ['userId' => $this->auth->getUid(), 'momentId' => $data['momentId']];
        if (! $db->insertOrUpdate('moment', $pk, $data)) {
            throw new Exception($db->errorString());
        }
        return true;
    }

    public function actionDelete($id) {
        $db = $this->server->getDb();
        $stmt = $db->prepare('UPDATE moment SET "deletedTime" = NOW() WHERE "momentId" = :id AND "userId" = :uid');
        if (!$stmt->execute([':id' => $id, ':uid' => $this->auth->getUid()])) {
            throw new Exception($db->errorString());
        }
        return true;
    }
} 
